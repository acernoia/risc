#include <systemc.h>
#include <string>
#include "RISC_v2.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<bool> clock;
    RISC_v2 risc;
    
    SC_CTOR(TestBench) : risc("risc")
    {
        SC_THREAD(stimulus_thread);
        SC_THREAD(clock_thread);
        risc.clock(this->clock);
        init_values();
    }

    int check() 
    {
       cout << endl;
       cout << "FINE SIMULAZIONE" << endl;
       cout << "CLOCK COUNT     : " << clock_count/2 << endl;
       cout << "SIMULATION TIME : " << clock_period*clock_count/2 << endl;
       return 1;
    }


  private:
   void stimulus_thread() 
   {
      cout << "STIMULUS START" << endl << endl;
      risc.reset.write(1);
   }
   void clock_thread() 
   {
     
     bool value = false;
     while(true) 
     {
            clock.write(value);
            value = !value;
            clock_count++;
            wait(clock_period/2,SC_NS);
         /*  if (value==true)
            {

            } */
      }
   }

    unsigned clock_count;
    unsigned clock_period;
    void init_values() 
    {
       //Inizializzazione del contatore dei cicli di clock
       clock_count=0;
       //Periodo di clock in nanosecondi
       clock_period=20;
    }


};

int sc_main(int argc, char* argv[])
{
    int n_istruzioni=0;
    ifstream f("/home/andreacernoia/Scrivania/cernoia_esd_rep/assembly.txt");
    string s;
    if(!f)
    {
        cout << "ERRORE : il file assembly indicato non esiste";
        return 1;
    }

    while(f.good()) //fino a quando c'è qualcosa da leggere ..
    {
        getline(f, s);
        n_istruzioni++;
    }
    f.close(); //chiude il file
    
    int sim_time=(n_istruzioni-1)*20;
  
    cout << "STAR TEST" << endl << endl;

    TestBench test_risc("test_risc");
    sc_set_time_resolution(1,SC_NS);
    sc_start(sim_time,SC_NS);

    return test_risc.check();
}
