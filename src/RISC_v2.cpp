#include <systemc.h>
#include "RISC_v2.hpp"

using namespace std;


SC_HAS_PROCESS(RISC_v2);

RISC_v2::RISC_v2(sc_module_name name):

  sc_module(name),
  alu("alu"), ctrl("ctrl"), dmem("dmem"), imem("imem"), 
  ls("ls"), mux_rf("mux_rf"), mux_alu1("mux_alu1"),
  mux_alu2("mux_alu2"), mux_tgt("mux_tgt"), mux_pc("mux_pc"),
  rf("rf"), pc("pc"), add("add"),
  inc("inc"), se_alu("se_alu"), se_pc("se_pc"),
  idecode("idecode")

   {
      //Collegamenti della ALU
      alu.src1(this->alu_in1);
      alu.src2(this->alu_in2);
      alu.func_alu(this->func_alu);
      alu.alu_out(this->alu_out);
      //Collegamenti di mux_alu1
      mux_alu1.X0(this->ls_out);
      mux_alu1.X1(this->rf_out1);
      mux_alu1.sel(this->sel_alu1);
      mux_alu1.Y(this->alu_in1);
      //Collegamenti di mux_alu2
      mux_alu2.X0(this->se_alu_out);
      mux_alu2.X1(this->rf_out2);
      mux_alu2.sel(this->sel_alu2);
      mux_alu2.Y(this->alu_in2);
      //Collegamenti di se_alu (sign extend vicino alla alu)
      se_alu.ingresso(this->istruzione_7lsb);
      se_alu.uscita(this->se_alu_out);
      //Collegamenti di ls (left_shift_6)
      ls.ingresso(this->istruzione_10lsb);
      ls.uscita(this->ls_out);
      //Collegamenti del register file
      rf.CLK(this->clock);
      rf.RST(this->reset);
      rf.WE_RF(this->enable_rf);
      rf.TGT_addr(this->rA); //campo istruzione rA
      rf.SRC1_addr(this->rB);  //campo istruzione rB
      rf.SRC2_addr(this->SRC2_addr);
      rf.TGT(this->target);
      rf.SRC1(this->rf_out1);
      rf.SRC2(this->rf_out2);
      //Collegamenti di mux_rf
      mux_rf.X0(this->rA);  //campo istruzione rA
      mux_rf.X1(this->rC);    //campo istruzione rC
      mux_rf.sel(this->sel_alu_rf);  
      mux_rf.Y(this->SRC2_addr);
      //Collegamenti di mux_tgt
      mux_tgt.X0(this->inc_out);
      mux_tgt.X1(this->alu_out);
      mux_tgt.X2(this->dmem_out);
      mux_tgt.sel(this->sel_tgt);
      mux_tgt.Y(this->target);
      //Collegamenti della data memory
      dmem.address(this->alu_out);
      dmem.datain(this->rf_out2);
      dmem.enable(this->enable_dmem);
      dmem.dataout(this->dmem_out);
      dmem.ready(this->ready_dmem);
      //Collegamenti del program counter
      pc.CLK(this->clock);
      pc.ingresso(this->pc_in);
      pc.uscita(this->pc_out);
      //Collegamenti dell'instruction memory
      imem.indirizzo(this->pc_out);
      imem.istruzione(this->istruzione);
      //Collegamenti del mux_pc
      mux_pc.X0(this->alu_out);
      mux_pc.X1(this->adder_out);
      mux_pc.X2(this->inc_out);
      mux_pc.sel(this->sel_pc);
      mux_pc.Y(this->pc_in);
      //Collegamenti dell'incrementatore del program counter
      inc.ingresso(this->pc_out);
      inc.uscita(this->inc_out);
      //Collegamenti del sommatore del program counter
      add.ingresso1(this->inc_out);
      add.ingresso2(this->se_pc_out);
      add.uscita(this->adder_out);
      //Collegamenti del sign extend del pc
      se_pc.ingresso(this->istruzione_7lsb);
      se_pc.uscita(this->se_pc_out);
      //Collegamenti della control unit
      ctrl.opcode(this->rOP);
      ctrl.eq(this->alu_out);
      ctrl.sel_ALU1(this->sel_alu1);
      ctrl.sel_ALU2(this->sel_alu2);
      ctrl.func_ALU(this->func_alu);
      ctrl.sel_RF(this->sel_alu_rf);
      ctrl.load_RF(this->enable_rf);
      ctrl.sel_TGT(this->sel_tgt);
      ctrl.write_mem(this->enable_dmem);
      ctrl.sel_PC(this->sel_pc);
      //Collegamenti del decodificatore di istruzioni
      idecode.ingresso(this->istruzione);
      idecode.rA(this->rA);
      idecode.rB(this->rB);
      idecode.rC(this->rC);
      idecode.rOP(this->rOP);
      idecode.lsb7(this->istruzione_7lsb);
      idecode.lsb10(this->istruzione_10lsb);
   }
