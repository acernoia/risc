#ifndef RISC_V2_HPP
#define RISC_V2_HPP

#include "ALU.hpp"
#include "control_unit.hpp"
#include "data_memory.hpp"
#include "programmatore.hpp"
#include "instruction.hpp"
#include "left_shift_6.hpp"
#include "MUX_2x3.hpp"
#include "MUX_2x16.hpp"
#include "MUX_3x16.hpp"
#include "PC.hpp"
#include "PC_add.hpp"
#include "PC_inc.hpp"
#include "reg_file.hpp"
#include "sign_extend.hpp"

using namespace std;

SC_MODULE(RISC_v2)
{
   sc_in<bool > clock;
   sc_signal<sc_uint<1> >  reset, enable_rf;
   sc_signal<sc_uint<16> > istruzione;
   sc_signal<sc_uint<7> >  istruzione_7lsb;
   sc_signal<sc_uint<3> >  rA, rB, rC, rOP;
   sc_signal<sc_uint<10> >  istruzione_10lsb;
   sc_signal<sc_uint<16> > alu_in1, alu_in2, alu_out;
   sc_signal<sc_uint<2> >  func_alu;
   sc_signal<sc_uint<16> > ls_out, se_alu_out, rf_out1, rf_out2;
   sc_signal<sc_uint<1> >  sel_alu1, sel_alu2, sel_alu_rf;
   sc_signal<sc_uint<2> >  sel_pc, sel_tgt;
   sc_signal<sc_uint<3> >  SRC2_addr;
   sc_signal<sc_uint<16> >  target;
   sc_signal<sc_uint<16> >  dmem_out;
   sc_signal<sc_uint<1> >   enable_dmem, ready_dmem;
   sc_signal<sc_uint<16> >  pc_in, pc_out, adder_out, inc_out, se_pc_out;
   sc_uint<3> var_rC;

   ALU alu;
   control_unit ctrl;
   data_memory dmem;
   programmatore imem;
   instruction idecode;
   left_shift_6 ls;
   MUX_2x3 mux_rf;
   MUX_2x16 mux_alu1;
   MUX_2x16 mux_alu2;
   MUX_3x16 mux_tgt;
   MUX_3x16 mux_pc;
   reg_file rf;
   PC pc;
   PC_add add;
   PC_inc inc;
   sign_extend se_alu;
   sign_extend se_pc;

   RISC_v2(sc_module_name name);

};


#endif
